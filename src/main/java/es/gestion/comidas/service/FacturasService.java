package es.gestion.comidas.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ArrayList;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import es.gestion.comidas.database.Cliente;
import es.gestion.comidas.database.DiaExtra;
import es.gestion.comidas.database.Factura;
import es.gestion.comidas.database.Menu;
import es.gestion.comidas.database.repositories.ClienteRepository;
import es.gestion.comidas.database.repositories.DiaExtraRepository;
import es.gestion.comidas.database.repositories.FacturaRepository;

@Service
public class FacturasService {
	
	private static final String PDF_FOLDER = "../pdf";
	
	private static BaseColor AZUL = new BaseColor(29, 92, 161);  
	private static BaseColor GRIS = new BaseColor(223, 223, 223); 
	private static BaseColor GRISCLARO = new BaseColor(221, 239, 241); 
	private static BaseColor AZULCLARO = new BaseColor(210, 219, 234); 
    
    @Autowired
    private ClienteRepository clienteRepository;
    
    @Autowired
    private FacturaRepository facturaRepository;
    
    @Autowired
    private DiaExtraRepository diaExtraRepository;
    
    public List<String> getListaAnyos() {
		File directory = new File(PDF_FOLDER);
		return listFilesForFolder(directory);
    }
    
    public List<String> getListaMeses(String anyo) {
		File directory = new File(PDF_FOLDER + "/" + anyo);
		return listFilesForFolder(directory);
    }
    
    public List<String> getListaFacturas(String anyo, String mes) {
		File directory = new File(PDF_FOLDER + "/" + anyo + "/" + mes);
		return listFilesForFolder(directory);
    }
    
    private List<String> listFilesForFolder(final File folder) {
    	ArrayList<String> lista = new ArrayList<String>(); 
    	if(!folder.exists())
    		return lista;
    	
        for (final File fileEntry : folder.listFiles()) {
            /*if (fileEntry.isDirectory()) {
                //lista.addAll(listFilesForFolder(fileEntry));
            } else {
                lista.add(fileEntry.getName());
            }*/
        	lista.add(fileEntry.getName());
        }
        return lista;
    }
	
	public int generarFacturas(String fecha) {
		int contador = 0;
		
		String[] fechaSplit = fecha.split("-");
		String anyo = fechaSplit[0];
		String mes = fechaSplit[1];
		String dia = fechaSplit[2];
		
		Date date;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
		} catch (ParseException e) {
			date = new Date();
		}  
		
		String carpeta = String.format("%s/%s/%s/", PDF_FOLDER, anyo, mes);
		File directory = new File(carpeta);
	    if (! directory.exists()){
	        directory.mkdirs();
	    }
		
		List<Cliente> clientes = clienteRepository.findAll();
		
		for(Cliente cliente : clientes) {
			Factura factura = facturaRepository.getOneByAnyoAndMesAndCliente(anyo, mes, cliente);
			if(factura == null) {
				factura = new Factura();
				factura.setAnyo(anyo);
				factura.setMes(mes);
				factura.setCliente(cliente);
				factura = facturaRepository.save(factura);
			}
			if(generarFacturaCliente(cliente, carpeta, String.format("%06d",  factura.getId()), date))
				contador++;
		}
		
		System.out.println("Generadas " + contador + " facturas");
		
		return contador;
	}
	
	public boolean generarFacturaCliente(Cliente cliente, String carpeta, String albaran, Date fecha) {
		String nombre = String.format("%s/%s.pdf", carpeta, cliente.getZona().getName()+"-"+cliente.getName().replace(" ", "-"));

		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream(nombre));
			document.open();
			
			// Ponemos una fuente con tamaño y color
			//Font font = FontFactory.getFont(FontFactory.HELVETICA, 12, BaseColor.BLACK);
			
			PdfPTable table = new PdfPTable(2);
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			table.setWidthPercentage(100);
			
			table.addCell(getElPuchero()); 
			table.addCell(getAlbaranCliente(albaran, new SimpleDateFormat("dd/MM/yyyy").format(fecha), cliente));
			
			document.add(table);
			
			document.add(new Paragraph( " " ));
			
			String mes = new SimpleDateFormat("MMMM", Locale.forLanguageTag("es-ES")).format(fecha).toUpperCase();
			String codigo = new SimpleDateFormat("MM").format(fecha);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(fecha);
			//int dias = calendar.getActualMaximum(Calendar.DATE);
			int dias = getNumDias(calendar, cliente);
			double precio = Double.parseDouble(cliente.getMenu().getPrecio().replace(",", "."));
			document.add(getCuenta(codigo, mes, dias, precio));
			
			document.add(new Paragraph( " " ));
			document.add(new Paragraph( " " ));
			document.add(new Paragraph( " " ));
			document.add(new Paragraph( " " ));
			document.add(new Paragraph( " " ));
			document.add(new Paragraph( " " ));
			
			document.add(getTotal(dias * precio, cliente.getTipo_pago()));

			document.add(new Paragraph( " " ));
			document.add(new Paragraph( " " ));
			document.add(new Paragraph( " " ));
			document.add(new Paragraph( " " ));
			
			document.add(getProteccionDatos());
			
			document.close();
			
			return true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	/*private PdfPCell addCellNoBorder(String text){
		PdfPCell cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setPhrase(new Phrase(text));
		return cell;
	}*/
	
	private PdfPCell getElPuchero() {
		PdfPCell cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.addElement(new Paragraph( "COMIDAS EL PUCHERO SL" ));
		cell.addElement(new Paragraph( "B02449460" ));
		cell.addElement(new Paragraph( "REAL 57" ));
		cell.addElement(new Paragraph( "02610 BONILLO, EL" ));
		cell.addElement(new Paragraph( "ALBACETE" ));
		cell.addElement(new Paragraph( "637435421  661775546" ));
		cell.addElement(new Paragraph( "comidaselpuchero@hotmail.com" ));
		return cell;
	}
	
	private PdfPTable getAlbaran(String num, String fecha) {
		PdfPTable table = new PdfPTable(2);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.setWidthPercentage(100);
		
		PdfPCell header = new PdfPCell();
		header.setColspan(2);
		header.setBorder(Rectangle.NO_BORDER);
		header.setBackgroundColor(AZUL);
		header.setHorizontalAlignment(Element.ALIGN_CENTER);
		Phrase albaranTitulo = new Phrase("ALBARÁN " + num, FontFactory.getFont(FontFactory.HELVETICA, 12, BaseColor.WHITE));
		header.setPhrase(albaranTitulo);
		table.addCell(header);
		
		table.addCell("Fecha");
		table.addCell(fecha);
		
		table.addCell("Código");
		table.addCell(""); 
		
		table.addCell("Vendedor");
		table.addCell(""); 
		
		table.addCell("A la atención");
		table.addCell(""); 
		
		return table;
	}
	
	private PdfPTable getCliente(Cliente cliente) {
		PdfPTable table = new PdfPTable(1);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.setWidthPercentage(100);
		
		PdfPCell header = new PdfPCell();
		header.setColspan(2);
		header.setBorder(Rectangle.NO_BORDER);
		header.setBackgroundColor(GRIS);
		header.setHorizontalAlignment(Element.ALIGN_CENTER);
		Phrase albaranTitulo = new Phrase("CLIENTE");
		header.setPhrase(albaranTitulo);
		table.addCell(header);
		
		PdfPCell cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.addElement(new Paragraph( cliente.getName() ));
		cell.addElement(new Paragraph( "" ));
		cell.addElement(new Paragraph( cliente.getDirecion() ));
		table.addCell(cell);
		
		return table;
	}
	
	private PdfPCell getAlbaranCliente(String num, String fecha, Cliente cliente) {
		PdfPCell cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		
		cell.addElement(getAlbaran(num, fecha));
		cell.addElement(getCliente(cliente));
		return cell;
	}
	
	private int getNumDias(Calendar fecha, Cliente cliente) {
		Calendar c = Calendar.getInstance();
	    c.set(Calendar.MONTH, fecha.get(Calendar.MONTH)); // may is just an example
	    c.set(Calendar.YEAR, fecha.get(Calendar.YEAR));
		int dias = 0;
	    int maxDayInMonth = c.getActualMaximum(Calendar.DAY_OF_MONTH);
	    for (int d = 1;  d <= maxDayInMonth;  d++) {
	        c.set(Calendar.DAY_OF_MONTH, d);
	        
	        // Primero buscamos si se ha marcado como un día especial
			DiaExtra diaExtra = diaExtraRepository.findOneByClienteAndFecha(cliente, c.getTime());
			if(diaExtra != null) {
				if(diaExtra.getIncremento() == -1) {
					// Este día no se cuenta, lo saltamos
				}
				else if(diaExtra.getIncremento() == 1) {
					// Este día no se cuenta siempre
					dias++;
				}
				continue; // Hacemos esto para que no compruebe el día de la semana
			}
	        
	        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
	        switch(dayOfWeek) {
	        case Calendar.MONDAY:
	        	dias += cliente.getL();
	        	break;
	        case Calendar.TUESDAY:
	        	dias += cliente.getM();
	        	break;
	        case Calendar.WEDNESDAY:
	        	dias += cliente.getX();
	        	break;
	        case Calendar.THURSDAY:
	        	dias += cliente.getJ();
	        	break;
	        case Calendar.FRIDAY:
	        	dias += cliente.getV();
	        	break;
	        case Calendar.SATURDAY:
	        	dias += cliente.getS();
	        	break;
	        case Calendar.SUNDAY:
	        	dias += cliente.getD();
	        	break;
	        }
	    }
	    return dias;
	}
	
	private PdfPTable getCuenta(String codigo, String mes, int numDias, double precio) {
		PdfPTable table = new PdfPTable(7);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.setWidthPercentage(100);
		try {
			table.setWidths(new float[] { 1, 5, 1, 2, 1, 1, 2 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		PdfPCell cuenta = new PdfPCell();
		cuenta.setColspan(7);
		cuenta.setBorder(Rectangle.NO_BORDER);
		cuenta.setBackgroundColor(GRISCLARO);
		cuenta.setPhrase(new Phrase("NUMERO DE CUENTA: ES63 3190 0002 0902 5130 9829"));
		table.addCell(cuenta);
		
		Stream.of("Código", "Concepto", "Cant.", "Precio", "DTO", "IVA", "Total")
	      .forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(AZULCLARO);
	        header.setBorder(Rectangle.NO_BORDER);
	        header.setPhrase(new Phrase(columnTitle, FontFactory.getFont(FontFactory.HELVETICA, 11, BaseColor.BLACK)));
	        table.addCell(header);
	    });
		
		table.addCell(codigo); 
		table.addCell("COMIDAS MES " + mes); 
		table.addCell(String.format("%d", numDias));
		table.addCell(String.format("%.2f €", precio));
		table.addCell("");
		table.addCell("");
		table.addCell(String.format("%.2f €", numDias * precio));
		
		return table;
	}
	
	private PdfPTable getTotalLeft(double total, String formaPago) {
		PdfPTable table = new PdfPTable(5);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.setWidthPercentage(100);
		
		Stream.of("Base", "% IVA", "Impuesto", "RE%", "Impuesto")
	      .forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(GRIS);
	        header.setBorder(Rectangle.NO_BORDER);
	        header.setPhrase(new Phrase(columnTitle));
	        table.addCell(header);
	    });

		table.addCell(String.format("%.2f €", total));
		table.addCell("Exento");
		table.addCell("");
		table.addCell("");
		table.addCell("");
		
		// Fila vacía
		table.addCell(" ");
		table.addCell("");
		table.addCell("");
		table.addCell("");
		table.addCell("");
		
		// Fila vacía
		table.addCell(" ");
		table.addCell("");
		table.addCell("");
		table.addCell("");
		table.addCell("");
		
		// Fila vacía
		table.addCell(" ");
		table.addCell("");
		table.addCell("");
		table.addCell("");
		table.addCell("");
		
		Stream.of("", "", "", "Imp. pendiente")
	      .forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(GRIS);
	        header.setBorder(Rectangle.NO_BORDER);
	        header.setPhrase(new Phrase(columnTitle));
	        if(!columnTitle.isEmpty())
	        	header.setColspan(2);
	        table.addCell(header);
	    });
		
		table.addCell("");
		table.addCell("");
		table.addCell("");
		table.addCell("");
		table.addCell(String.format("%.2f €", total));
		
		PdfPCell pago = new PdfPCell();
		pago.setBorder(Rectangle.NO_BORDER);
		pago.setColspan(5);
		pago.setPhrase(new Phrase("Forma pago: " + formaPago));
		table.addCell(pago);
		
		return table;
	}
	
	private PdfPTable getTotalRight(double total) {
		PdfPTable table = new PdfPTable(2);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.setWidthPercentage(100);
		
		PdfPCell header = new PdfPCell();
        header.setBackgroundColor(GRIS);
        header.setBorder(Rectangle.NO_BORDER);
        
        header.setPhrase(new Phrase("Bruto"));
        table.addCell(header);
		table.addCell(String.format("%.2f €", total));

        header.setPhrase(new Phrase("Descuentos"));
        table.addCell(header);
		table.addCell("");
        
        header.setPhrase(new Phrase("Neto"));
        table.addCell(header);
		table.addCell(String.format("%.2f €", total));

        header.setPhrase(new Phrase("Impuestos"));
        table.addCell(header);
		table.addCell("");

        header.setPhrase(new Phrase("Portes"));
        table.addCell(header);
		table.addCell("");

        header.setPhrase(new Phrase("Retención"));
        table.addCell(header);
		table.addCell("");

        header.setPhrase(new Phrase("R.E."));
        table.addCell(header);
		table.addCell("");

        header.setPhrase(new Phrase("TOTAL", FontFactory.getFont(FontFactory.HELVETICA, 12, BaseColor.WHITE)));
        header.setBackgroundColor(AZUL);
        table.addCell(header);
		table.addCell(String.format("%.2f €", total));
		
		return table;
	}
	
	private PdfPTable getTotal(double total, String formaPago) {
		PdfPTable table = new PdfPTable(3);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.setWidthPercentage(100);
		try {
			table.setWidths(new float[] { 5, 1, 3 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		table.addCell(getTotalLeft(total, formaPago));
		table.addCell(" ");
		table.addCell(getTotalRight(total));
		
		return table;
	}
	
	private PdfPTable getProteccionDatos() {
		PdfPTable table = new PdfPTable(1);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.setWidthPercentage(100);
		
		String texto = "En cumplimiento de lo establecido en la Ley Orgánica 15/1999, "
				+ "de 13 de diciembre, de Protección de Datos de Carácter Personal, "
				+ "le comunicamos que los datos que usted nos facilite quedarán "
				+ "incorporados en los ficheros correspondientes, con el fin de "
				+ "poderle prestar nuestros servicios, con el compromiso de "
				+ "tratar de forma confidencial los datos de carácter personal "
				+ "facilitados y a no comunicar o ceder dichar información a terceros";
		
		table.addCell(new Phrase(texto, FontFactory.getFont(FontFactory.HELVETICA, 8, BaseColor.BLACK)));
		
		return table;
	}
	
	public int facturaTest() {
		// Ver ejemplo en: https://www.baeldung.com/java-pdf-creation
		
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("PucheroTest.pdf"));
			document.open();
			
			// Ponemos una fuente con tamaño y color
			//Font font = FontFactory.getFont(FontFactory.HELVETICA, 12, BaseColor.BLACK);
			
			Cliente clienteTest = new Cliente();
			clienteTest.setName("ANGELA HURTADO");
			clienteTest.setDirecion("02300 ALCARAZ ALBACETE");
			clienteTest.setTipo_pago("Contado");
			Menu menu = new Menu();
			menu.setPrecio("3,54");
			
			PdfPTable table = new PdfPTable(2);
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			table.setWidthPercentage(100);
			
			table.addCell(getElPuchero()); 
			table.addCell(getAlbaranCliente("X-202002086", "29/10/2021", clienteTest));
			
			document.add(table);
			
			document.add(new Paragraph( " " ));
			
			double precio = Double.parseDouble(menu.getPrecio().replace(",", "."));
			document.add(getCuenta("10", "OCTUBRE", 31, precio));
			
			document.add(new Paragraph( " " ));
			document.add(new Paragraph( " " ));
			
			document.add(getTotal(31 * precio, clienteTest.getTipo_pago()));
			
			document.add(getProteccionDatos());
			
			document.close();
			
			return 1;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public byte[] descargaTest() throws IOException {
		File file = new File("PucheroTest.pdf");
		return Files.readAllBytes(file.toPath());
	}
	
	public byte[] descarga(String anyo, String mes, String id) throws IOException {
		File file = new File(PDF_FOLDER + "/" + anyo + "/" + mes + "/" + id);
		return Files.readAllBytes(file.toPath());
	}

}
