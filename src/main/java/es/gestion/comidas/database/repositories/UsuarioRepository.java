package es.gestion.comidas.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import es.gestion.comidas.database.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

	Usuario findByName(String name);
	
}
