package es.gestion.comidas.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import es.gestion.comidas.database.Menu;


//Hereda metodos parecidos a crud del JpaRepository
public interface MenuRepository extends JpaRepository <Menu, Integer> {
	
	List<Menu> findAll();
    //List<Menu> findAllById(Integer id);
    

}
