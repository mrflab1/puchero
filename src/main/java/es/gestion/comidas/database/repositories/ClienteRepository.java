package es.gestion.comidas.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import es.gestion.comidas.database.Cliente;
import es.gestion.comidas.database.Menu;
import es.gestion.comidas.database.Zona;

public interface ClienteRepository extends JpaRepository <Cliente, Integer> {

	List<Cliente> findAllByZona(Zona zona);
	
	List<Cliente> findByName(String name);
	//List<Cliente> findAllByNombre(Cliente nombre);
	
	List<Cliente> findAllByMenu(Menu menu);
}
