package es.gestion.comidas.database.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import es.gestion.comidas.database.Cliente;
import es.gestion.comidas.database.DiaExtra;

public interface DiaExtraRepository extends JpaRepository <DiaExtra, Integer> {

	List<DiaExtra> findAllByCliente(Cliente cliente);
	
	DiaExtra findOneByClienteAndFecha(Cliente cliente, Date date);
	
}
