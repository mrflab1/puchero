package es.gestion.comidas.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import es.gestion.comidas.database.Cliente;
import es.gestion.comidas.database.Factura;

public interface FacturaRepository extends JpaRepository <Factura, Integer> {

	Factura getOneByAnyoAndMesAndCliente(String anyo, String mes, Cliente cliente);
	
}
