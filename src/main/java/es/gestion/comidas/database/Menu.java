package es.gestion.comidas.database;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//import javax.validation.constraints.*;


//clase que están mapeadas contra una tabla de la base de datos
@Entity 

public class Menu {
	
	//diferenciar a la entidad del resto
	@Id
	//columna de base de datos con incremento automático y permite que la base de datos genere un nuevo valor con cada operación de inserción
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private Integer id;
	

	//@Min(1)
	//@Max(500)
	private String precio;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}
	
	

}
