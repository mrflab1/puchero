package es.gestion.comidas.database;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;



//@Table(name = "cliente") //lo usan en algunos ejemplos

//clase que están mapeadas contra una tabla de la base de datos
@Entity //Significa que esta clase entidad representa una tabla de datos relacional
public class Cliente {
	
	
	//diferenciar a la entidad del resto //sirve para indicar el atributo que representa la PK de la tabla.
		@Id 
		//columna de base de datos con incremento automático y permite que la base de datos genere un nuevo valor con cada operación de inserción
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		
		private Integer id;

		private String name;
		
		private String direcion;
		
		private Integer telefono;
		
		@ManyToOne
		private Menu menu;

		private Integer l;
		
		private Integer m;
		
		private Integer x;
		
		private Integer j;
		
		private Integer v;
		
		private Integer s;
		
		private Integer d;
		
		private Integer sal;
		
		private Integer diabetico;
		
		private Integer triturado;
		
		private String tipo_pago;
		
		@ManyToOne
		private Zona zona;
		
		private String caracteristicas;
		
		//@Transient
		//private List<DiaExtra> diasExtras;
		
		@Transient
		private Boolean comeHoy;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDirecion() {
			return direcion;
		}

		public void setDirecion(String direcion) {
			this.direcion = direcion;
		}

		public Integer getTelefono() {
			return telefono;
		}

		public void setTelefono(Integer telefono) {
			this.telefono = telefono;
		}


		public Integer getL() {
			return l;
		}

		public void setL(Integer l) {
			this.l = l;
		}

		public Integer getM() {
			return m;
		}

		public void setM(Integer m) {
			this.m = m;
		}

		public Integer getX() {
			return x;
		}

		public void setX(Integer x) {
			this.x = x;
		}

		public Integer getJ() {
			return j;
		}

		public void setJ(Integer j) {
			this.j = j;
		}

		public Integer getV() {
			return v;
		}

		public void setV(Integer v) {
			this.v = v;
		}

		public Integer getS() {
			return s;
		}

		public void setS(Integer s) {
			this.s = s;
		}

		public Integer getD() {
			return d;
		}

		public void setD(Integer d) {
			this.d = d;
		}

		public Integer getSal() {
			return sal;
		}

		public void setSal(Integer sal) {
			this.sal = sal;
		}

		public Integer getDiabetico() {
			return diabetico;
		}

		public void setDiabetico(Integer diabetico) {
			this.diabetico = diabetico;
		}

		public Integer getTriturado() {
			return triturado;
		}

		public void setTriturado(Integer triturado) {
			this.triturado = triturado;
		}

		public String getTipo_pago() {
			return tipo_pago;
		}

		public void setTipo_pago(String tipo_pago) {
			this.tipo_pago = tipo_pago;
		}

		public Zona getZona() {
			return zona;
		}

		public void setZona(Zona zona) {
			this.zona = zona;
		}
		
		public Menu getMenu() {
			return menu;
		}

		public void setMenu(Menu menu) {
			this.menu = menu;
		}

		public String getCaracteristicas() {
			return caracteristicas;
		}

		public void setCaracteristicas(String caracteristicas) {
			this.caracteristicas = caracteristicas;
		}

		/*public List<DiaExtra> getDiasExtras() {
			return diasExtras;
		}

		public void setDiasExtras(List<DiaExtra> diasExtras) {
			this.diasExtras = diasExtras;
		}*/
		
		public Boolean getComeHoy() {
			return comeHoy;
		}

		public void setComeHoy(Boolean comeHoy) {
			this.comeHoy = comeHoy;
		}
		

}


