package es.gestion.comidas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

//clase que hace ejecutar nuestro proyecto en servidor

@SpringBootApplication
public class ComidaApplication extends SpringBootServletInitializer {
//public class ComidaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComidaApplication.class, args);
	}
	
	/*@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(ComidaApplication.class);
    }*/

}
