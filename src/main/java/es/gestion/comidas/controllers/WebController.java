package es.gestion.comidas.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import es.gestion.comidas.database.Cliente;
import es.gestion.comidas.database.DiaExtra;
import es.gestion.comidas.database.Menu;
import es.gestion.comidas.database.Usuario;
import es.gestion.comidas.database.Zona;
import es.gestion.comidas.database.repositories.ClienteRepository;
import es.gestion.comidas.database.repositories.DiaExtraRepository;
import es.gestion.comidas.database.repositories.MenuRepository;
import es.gestion.comidas.security.SecurityService;
import es.gestion.comidas.service.FacturasService;


//clase controladora responsable de preparar los datos que serán mostrados por la vista así como seleccionar la vista correcta a mostrar
@Controller //que sera una clase donde vamos a definir puntos de entrada para las peticiones web
//@RequestMapping //Añadida para el CRUD Cliente de listar aunque funciona sin el
public class WebController {
	
    @Autowired
    private SecurityService securityService;
    
    @Autowired
    private FacturasService facturasService;
    
    @Autowired
    private ClienteRepository clienteRepository;
    
    @Autowired
    private DiaExtraRepository diaExtraRepository;
    
    //buscar un objeto manejado (beans) que implementen determinada interfaz para hacer referencia a el
    @Autowired
    private MenuRepository menuRepository;
    
    @GetMapping("/") //procesar la solicitud de obtención raiz index
	public String homePage(Model model) {
		Usuario usuario = securityService.getUsuario();
		
		model.addAttribute("appName", usuario.getName());
		
		ArrayList<Cliente> clientes = new ArrayList<>();
		for(Zona zona : usuario.getZonas()) {
			clientes.addAll(clienteRepository.findAllByZona(zona));
		}
		model.addAttribute("clientes", clientes);
		
		
		return "index"; 
	}
	

	
	@GetMapping("/registration") //procesar la solicitud de obtención registro
    public String registration(Model model) {
        if (securityService.isAuthenticated()) {
            return "redirect:/";
        }

        model.addAttribute("userForm", new Usuario());

        return "registration";
    }
	
	@GetMapping("/login") //procesar la solicitud de obtención login
    public String login(Model model, String error, String logout) {
		String s = (new BCryptPasswordEncoder()).encode("2");
        if (securityService.isAuthenticated()) {
            return "redirect:/";
        }

        if (error != null)
            model.addAttribute("error", "Usuario o contraseña no válidos");

        if (logout != null)
            model.addAttribute("message", "Desconectado correctamente");
        

        return "login";
    }
	
	 @GetMapping("/volver")
	 public String volver() {
	  return "index";
	 }
	
	//@GetMapping("/listar") //procesar la solicitud de obtención cliente
	@RequestMapping("/listar")
	public String listar(Model model) {
		/*List<Cliente>clientes=service.listar();
		model.addAttribute("clientes", clientes);
		return "listar"; */
		
		Usuario usuario = securityService.getUsuario();
		
		ArrayList<Cliente> clientes = new ArrayList<>();
		for(Zona zona : usuario.getZonas()) {
			clientes.addAll(clienteRepository.findAllByZona(zona));
		}
		for(Cliente c : clientes) {
			//c.setDiasExtras(diaExtraRepository.findAllByCliente(c));
			c.setComeHoy(clienteComeHoy(c));
		}
		model.addAttribute("clientes", clientes);
		
		return "listar";
	}
	
	private boolean clienteComeHoy(Cliente cliente) {
		Calendar c = Calendar.getInstance();
		// Primero buscamos si se ha marcado como un día especial
		DiaExtra diaExtra = diaExtraRepository.findOneByClienteAndFecha(cliente, c.getTime());
		if(diaExtra != null) {
			if(diaExtra.getIncremento() == -1)
				return false;
			else if(diaExtra.getIncremento() == 1)
				return true;
		}
		
		// Después comprobamos el día en el que estamos
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        switch(dayOfWeek) {
        case Calendar.MONDAY:
        	return cliente.getL() == 1;
        case Calendar.TUESDAY:
        	return cliente.getM() == 1;
        case Calendar.WEDNESDAY:
        	return cliente.getX() == 1;
        case Calendar.THURSDAY:
        	return cliente.getJ() == 1;
        case Calendar.FRIDAY:
        	return cliente.getV() == 1;
        case Calendar.SATURDAY:
        	return cliente.getS() == 1;
        case Calendar.SUNDAY:
        	return cliente.getD() == 1;
        }
        
        return false; // Aquí no debería llegar nunca
	}
	

	
	//sera llamada cuando haya una petición a “/index”
	//La cadena devuelta sera la plantilla Thymeleaf devuelta, de tal manera que la llamada  función inicio  devolverá la plantilla “index.html”
	@RequestMapping("/index")
	public String index() {

		return "index"; 
	}
	
	 
	
	 @GetMapping("/save/{id}")
	 public String showSave(@PathVariable("id") Integer id, Model model) {
		 //obtenemos el usuario que entra para despues saber sus zonas
		 Usuario usuario = securityService.getUsuario();
		 
		 //añadimos al modelo las zonas del usuario
		 model.addAttribute("zonas", usuario.getZonas());
		 
		 //añadimos al modelo los menus que se mostraran en el formulario al añadir cliente
		 model.addAttribute("menus", menuRepository.findAll());

		 //Si el id existe cogemos su id, si no creamos uno nuevo creo
		 if (id != null && id != 0) {
			 model.addAttribute("cliente", clienteRepository.findById(id).get());
		 } else {
			 model.addAttribute("cliente", new Cliente());
		 }
		 
		 //vamos a save.html
		 return "save";
	 }
	 
	 @PostMapping("/save")
	 public String save(Cliente cliente, Model model) {
		 //cogemos el cliente rellenado en el formulario y lo guardamos en la base de datos
		 clienteRepository.save(cliente);
		 
		    //obtenemos el usuario que entra para despues saber sus zonas
			Usuario usuario = securityService.getUsuario();
			
			//creamos una lista de clientes donde añadiremos los de las zonas para añadirlo actualizado al modelo y se liste con el nuevo añadido
			ArrayList<Cliente> clientes = new ArrayList<>();
			for(Zona zona : usuario.getZonas()) {
				clientes.addAll(clienteRepository.findAllByZona(zona));
			}
			for(Cliente c : clientes) {
				//c.setDiasExtras(diaExtraRepository.findAllByCliente(c));
				c.setComeHoy(clienteComeHoy(c));
			}
			model.addAttribute("clientes", clientes);
		  
		  return "listar";
		 
	 // return "redirect:/";
	 }
	 
	 
	 @GetMapping("/delete/{id}")
	 public String delete(@PathVariable Integer id, Model model) {
	  clienteRepository.deleteById(id);
	  
		Usuario usuario = securityService.getUsuario();
		
		ArrayList<Cliente> clientes = new ArrayList<>();
		for(Zona zona : usuario.getZonas()) {
			clientes.addAll(clienteRepository.findAllByZona(zona));
		}
		for(Cliente c : clientes) {
			//c.setDiasExtras(diaExtraRepository.findAllByCliente(c));
			c.setComeHoy(clienteComeHoy(c));
		}
		model.addAttribute("clientes", clientes);
	  
	  return "listar";
	  
	 // return "redirect:/";
	 }


	 
	 	@GetMapping("/listarMenu")
		public String listarMenu(Model model) {
			
			
			ArrayList<Menu> menus = new ArrayList<>();
			
			menus.addAll(menuRepository.findAll());

			model.addAttribute("menus", menus);

			
			return "listarMenu";
		}
	 	
		 @GetMapping("/saveMenu/{id}")
		 public String showSaveMenu(@PathVariable("id") Integer id, Model model) {
		  if (id != null && id != 0) {
			  model.addAttribute("menu", menuRepository.findById(id));
		  } else {
		   model.addAttribute("menu", new Menu());
		  }
		  
		  return "saveMenu";
		 }
		 
		 @PostMapping("/saveMenu")
		 public String saveMenu(Menu menu, Model model) {
			 menuRepository.save(menu);
			 
			 ArrayList<Menu> menus = new ArrayList<>();
				
				menus.addAll(menuRepository.findAll());

				model.addAttribute("menus", menus);
			 
		  return "listarMenu";	 
		  //return "redirect:/";
		 }
		 
		 @GetMapping("/deleteMenu/{id}")
		 public String deleteMenu(@PathVariable Integer id, Model model) {
			 //CONTROLAR QUE LO QUE SE VA A BORRAR NO EXISTA EN ALGUN CLIENTE, SI NO DARIA EXCEPCION
			 Menu menu = menuRepository.getOne(id);
			 if(!clienteRepository.findAllByMenu(menu).isEmpty()) {
				 model.addAttribute("error", "No se puede eliminar porque existen clientes con este menú");
			 }
			 else {
				  menuRepository.deleteById(id);
			 }
			 
			ArrayList<Menu> menus = new ArrayList<>();
			
			menus.addAll(menuRepository.findAll());

			model.addAttribute("menus", menus);
		  
		  return "listarMenu";
		// return "redirect:/";
		  
		 }	 
		
		
		 // METER 1 en incremento FIJARSE EN EL GUARDAR DEL SAVE de editar clientes, el boton guardar que debe ser parecido 
		 @GetMapping("/saveDiaE/{id}")
		 public String showSaveDiaE(@PathVariable("id") Integer id, Model model) {	
			 
			 //JOptionPane.showMessageDialog(null, "Dia extra añadido");
				
			// Primero buscamos el cliente
			Cliente cliente = clienteRepository.findById(id).get();
				
			// Después buscamos si ya tiene algo para el día de hoy
			Date date = new Date();
			DiaExtra diaExtra = diaExtraRepository.findOneByClienteAndFecha(cliente, date);
			 
			// Si está vacío 
			if(diaExtra == null) {
				diaExtra = new DiaExtra();
				diaExtra.setCliente(cliente);
				diaExtra.setFecha(date);
			}
			diaExtra.setIncremento(1);
			diaExtraRepository.save(diaExtra);
			 			 
			 //Si el mismo dia ha sido marcado cogemos su id, si no creamos uno nuevo creo
			//cogemos el dia extra metiendole el 1 y lo guardamos en la base de datos
			 /*if (id != null && id != 0) {
				 DiaExtra diaextra = new DiaExtra();
				 //model.addAttribute("diaextra", diaExtraRepository.findById(id).get());
				 diaextra.setIncremento(1);
				 diaextra.setCliente(clienteRepository.findById(id).get());
				 //FALTA METER DIA ACTUAL
				 diaExtraRepository.save(diaextra);
				 //model.addAttribute("diaextra", diaExtraRepository.findAllByCliente(clientes));
				 
			 } else {
				 DiaExtra diaextra = new DiaExtra();
				 diaextra.setIncremento(1);
				 diaextra.setCliente(clienteRepository.findById(id).get());
				 diaExtraRepository.save(diaextra);
				 //model.addAttribute("diaextra", new DiaExtra());
			 }*/
			 
			 //ERROR java.awt.HeadlessException: null
			// JOptionPane.showMessageDialog(null, "Dia extra añadido");
			 

			    Usuario usuario = securityService.getUsuario();
				ArrayList<Cliente> clientes = new ArrayList<>();
				for(Zona zona : usuario.getZonas()) {
					clientes.addAll(clienteRepository.findAllByZona(zona));
				}
				for(Cliente c : clientes) {
					//c.setDiasExtras(diaExtraRepository.findAllByCliente(c));
					c.setComeHoy(clienteComeHoy(c));
				}
				model.addAttribute("clientes", clientes);
			 
			 return "/listar";
		 }
		 
		  @GetMapping("/saveDiaQ/{id}")
		 public String saveQ(@PathVariable("id") Integer id, Model model) {
			  
			//JOptionPane.showMessageDialog(null, "Dia quitado");
			  
				// Primero buscamos el cliente
				Cliente cliente = clienteRepository.findById(id).get();
					
				// Después buscamos si ya tiene algo para el día de hoy
				Date date = new Date();
				DiaExtra diaExtra = diaExtraRepository.findOneByClienteAndFecha(cliente, date);
				 
				// Si está vacío 
				if(diaExtra == null) {
					diaExtra = new DiaExtra();
					diaExtra.setCliente(cliente);
					diaExtra.setFecha(date);
				}
				diaExtra.setIncremento(-1);
				diaExtraRepository.save(diaExtra);
			 
			 			 
			 //Si el mismo dia ha sido marcado cogemos su id, si no creamos uno nuevo creo
			//cogemos el dia extra metiendole el -1 y lo guardamos en la base de datos
			  /*if (id != null && id != 0) {
				 DiaExtra diaextra = new DiaExtra();
				 //model.addAttribute("diaextra", diaExtraRepository.findById(id).get());
				 diaextra.setIncremento(-1);
				 diaextra.setCliente(clienteRepository.findById(id).get());
				 //FALTA METER DIA ACTUAL
				 diaExtraRepository.save(diaextra);
				 //model.addAttribute("diaextra", diaExtraRepository.findAllByCliente(clientes));
				 
			 } else {
				 DiaExtra diaextra = new DiaExtra();
				 diaextra.setIncremento(-1);
				 diaextra.setCliente(clienteRepository.findById(id).get());
				 diaExtraRepository.save(diaextra);
				 //model.addAttribute("diaextra", new DiaExtra());
			 }*/
			 
			 //ERROR java.awt.HeadlessException: null
			// JOptionPane.showMessageDialog(null, "Dia quitado");
			 
			 
				Usuario usuario = securityService.getUsuario();
				ArrayList<Cliente> clientes = new ArrayList<>();
				for(Zona zona : usuario.getZonas()) {
					clientes.addAll(clienteRepository.findAllByZona(zona));
				}
				for(Cliente c : clientes) {
					//c.setDiasExtras(diaExtraRepository.findAllByCliente(c));
					c.setComeHoy(clienteComeHoy(c));
				}
				model.addAttribute("clientes", clientes);
			 
			 return "/listar";
		 }

	@GetMapping("/facturas") 
    public String facturas(Model model) {
        if (!securityService.isAuthenticated()) {
            return "redirect:/";
        }

        model.addAttribute("anyos", facturasService.getListaAnyos());
        
        return "facturas";
    }	
	
	@PostMapping("/facturas") 
    public String generarFacturas(String fecha, Model model) {

		int contador = facturasService.generarFacturas(fecha);
		model.addAttribute("facturas", contador);
		model.addAttribute("fecha", fecha);

        model.addAttribute("anyos", facturasService.getListaAnyos());
		
        return "/facturas";
    }
	
	@GetMapping("/facturas/descarga/{anyo}") 
    public String facturasDescargaAnyo(@PathVariable String anyo, Model model) {
        if (!securityService.isAuthenticated()) {
            return "redirect:/";
        }

        model.addAttribute("anyo", anyo);
        model.addAttribute("meses", facturasService.getListaMeses(anyo));
        
        return "facturasAnyo";
    }
	
	@GetMapping("/facturas/descarga/{anyo}/{mes}") 
    public String facturasDescargaAnyo(@PathVariable String anyo, @PathVariable String mes, Model model) {
        if (!securityService.isAuthenticated()) {
            return "redirect:/";
        }

        model.addAttribute("anyo", anyo);
        model.addAttribute("mes", mes);
        List<String> facturas = facturasService.getListaFacturas(anyo, mes);
        model.addAttribute("facturas", facturas);
        
        /*ArrayList<String> nombres = new ArrayList<String>(); 
        for(String factura : facturas) {
        	Cliente cliente = clienteRepository.getOne(null)
        }*/
        
        return "facturasMes";
    }	
	
	@GetMapping("/facturas/descarga/{anyo}/{mes}/{id}")
	public ResponseEntity<?> getPDF(@PathVariable String anyo, @PathVariable String mes, @PathVariable String id, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String attachment = "attachment; filename=" + id + ".pdf";
		return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, attachment)
                .contentType(MediaType.APPLICATION_PDF) 
                .body(facturasService.descarga(anyo, mes, id));
	}
	
	@PostMapping("/facturas/test") 
    public String facturasTest(Model model) {

		facturasService.facturaTest();
		
        return "facturas";
    }
	
	@GetMapping("/facturas/test/descarga")
	public ResponseEntity<?> getTestPDF(HttpServletRequest request, HttpServletResponse response) throws IOException {
		return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=PucheroTest.pdf") 
                .contentType(MediaType.APPLICATION_PDF) 
                .body(facturasService.descargaTest());
	}
	
	@GetMapping("/pwd")
	public String clave(Model model) {
		
        return "pwd";
    } 
	
	@PostMapping("/pwd") 
    public String generarClave(String pwd, Model model) {

		String result = (new BCryptPasswordEncoder()).encode(pwd);
		model.addAttribute("pwd", result);
		
        return "/pwd";
    }
		 
}


